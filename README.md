# SWIS Iframe-fixer for GDPR (AVG)

![Run Status](https://img.shields.io/bitbucket/pipelines/swisnl/avg-safe-iframes/master?style=flat-square)

This package will look for `<iframe>`-elements in a HTML-snippet, and "fix" it in order to play nicely with our cookiebar.

## Install
`composer require swisnl/avg-safe-iframes`

## What it does
1. Replace the src-attribute with data-src (so the iframe won't load right away)
2. Adds a class to the element (`cookie-settings-*`), so the cookiebar knows if it complies with the users preferences. The default class is for tracking-cookies.

## How to use it
Via the class:
```php
$avgSafeIframe = new \Swis\AvgSafeIframes\AvgSafeIframes();

$text = '<div><iframe class="foo-bar" src="http:///www.example.com"></iframe></div>';

return $avgSafeIframe->fix($text);

// <div><iframe class="foo-bar cookie-settings-tracking" data-src="http:///www.example.com"></iframe></div>
```

Via the helper (in a blade-template, for example):
```
{!! safeAvgIframes($text) !!}
```

In both scenario's, you could provide a second parameter to add a different class. Please use one of the `AvgSafeIframes::MODE*_` constants:

* `MODE_TRACKING`
* `MODE_STATS`
* `MODE_STATS_AND_TRACKING`

## Todo
* Add a (Laravel) middleware so you won't have to manually use the helper  
