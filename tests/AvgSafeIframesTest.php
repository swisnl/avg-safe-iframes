<?php

class AvgSafeIframesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     */
    public function it_converts_iframe_src_to_data_src_and_adds_class()
    {
        $avgSafeIframe = new \Swis\AvgSafeIframes\AvgSafeIframes();

        $text = '<div>
                    <iframe class="foo-bar" src="http:///www.example.com"></iframe>
                    <span>test</span>
                </div>';
        $fixed = $avgSafeIframe->fix($text, \Swis\AvgSafeIframes\AvgSafeIframes::MODE_STATS);

        $this->assertEquals(
            '<div>
                    <iframe class="foo-bar cookie-settings-stats" data-src="http:///www.example.com"></iframe>
                    <span>test</span>
                </div>',
            $fixed
        );

        $fixed = $avgSafeIframe->fix($text, \Swis\AvgSafeIframes\AvgSafeIframes::MODE_STATS_AND_TRACKING);

        $this->assertEquals(
            '<div>
                    <iframe class="foo-bar cookie-settings-stats-and-tracking" data-src="http:///www.example.com"></iframe>
                    <span>test</span>
                </div>',
            $fixed
        );

        $fixed = $avgSafeIframe->fix($text, \Swis\AvgSafeIframes\AvgSafeIframes::MODE_TRACKING);

        $this->assertEquals(
            '<div>
                    <iframe class="foo-bar cookie-settings-tracking" data-src="http:///www.example.com"></iframe>
                    <span>test</span>
                </div>',
            $fixed
        );
    }

    /**
     * @test
     */
    public function it_removes_iframes_as_fallback()
    {
        $avgSafeIframe = new \Swis\AvgSafeIframes\AvgSafeIframes();

        $text = '<div>
                    <this-tag-will-not-be-closed><p>Test</p><iframe class="foo-bar" src="http:///www.example.com"></iframe><span>test</span>
                </div>';
        $fixed = $avgSafeIframe->fix($text, \Swis\AvgSafeIframes\AvgSafeIframes::MODE_STATS);

        $this->assertEquals(
            '<div>
                    <this-tag-will-not-be-closed><p>Test</p><span>test</span>
                </div>',
            $fixed
        );
    }

    /**
     * @test
     */
    public function it_throws_exception_when_invalid_mode()
    {
        $avgSafeIframe = new \Swis\AvgSafeIframes\AvgSafeIframes();

        $this->expectExceptionMessage('Could not find mode thisModeDoesNotExist');
        $avgSafeIframe->fix('<div><iframe src="http://www.example.com"></iframe></div>', 'thisModeDoesNotExist');
    }

    /**
     * @test
     */
    public function it_registers_helper_function()
    {
        $text = '<div>
                    <iframe class="foo-bar" src="http:///www.example.com"></iframe>
                    <span>test</span>
                </div>';

        $avgSafeIframe = new \Swis\AvgSafeIframes\AvgSafeIframes();

        $this->assertEquals(
            $avgSafeIframe->fix($text, \Swis\AvgSafeIframes\AvgSafeIframes::MODE_STATS),
            avgSafeIframes($text, \Swis\AvgSafeIframes\AvgSafeIframes::MODE_STATS)
        );
    }
}
