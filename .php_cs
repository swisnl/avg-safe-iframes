<?php

$finder = PhpCsFixer\Finder::create()
    ->in([
        __DIR__.'/src',
        __DIR__.'/tests',
    ])
;

return Swis\PhpCsFixer\Config\Factory::create()->setFinder($finder);
