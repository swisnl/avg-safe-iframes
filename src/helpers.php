<?php

use Swis\AvgSafeIframes\AvgSafeIframes;

if (!function_exists('avgSafeIframes')) {
    function avgSafeIframes(string $html, string $mode = AvgSafeIframes::MODE_TRACKING)
    {
        return (new AvgSafeIframes())->fix($html, $mode);
    }
}
