<?php

namespace Swis\AvgSafeIframes;

use DOMDocument;
use DOMElement;
use DOMXPath;

class AvgSafeIframes
{
    const MODE_STATS = 'stats';
    const MODE_TRACKING = 'tracking';
    const MODE_STATS_AND_TRACKING = 'stats-and-tracking';

    /**
     * @param $html
     * @param string $mode
     *
     * @return string
     */
    public function fix($html, string $mode = self::MODE_TRACKING)
    {
        try {
            return $this->findAndReplaceIframesAsDomDocument($html, $mode);
        } catch (\InvalidArgumentException $exception) {
            throw $exception;
        } catch (\Throwable $throwable) {
            return $this->removeIframes($html);
        }
    }

    /**
     * @param $html
     * @param string $mode
     *
     * @return string
     */
    protected function findAndReplaceIframesAsDomDocument($html, string $mode)
    {
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->loadHTML(
            '<?xml encoding="utf-8" ?>'.$html,
            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
        ); // force UTF-8 (see https://stackoverflow.com/questions/8218230/php-domdocument-loadhtml-not-encoding-utf-8-correctly)

        $xpath = new DOMXPath($dom);
        $iframes = $xpath->query('//iframe');

        foreach ($iframes as $iframe) {
            $this->fixIframe($iframe, $mode);
        }

        return substr(str_replace('<?xml encoding="utf-8" ?>', '', $dom->saveHTML()), 0, -1);
    }

    /**
     * @param DOMElement $iframe
     * @param string     $mode
     */
    protected function fixIframe(DOMElement $iframe, string $mode)
    {
        // change src to data-src (which the SWIS-AVG tag should return to a regular src if AVG-consent is given)
        $iframe->setAttribute('data-src', $iframe->getAttribute('src'));
        $iframe->removeAttribute('src');

        // add class
        $classAttribute = $iframe->getAttribute('class');
        $classAttribute .= ' '.$this->getClassForMode($mode);

        $iframe->setAttribute('class', $classAttribute);
    }

    /**
     * @param $html
     *
     * @return string
     */
    protected function removeIframes($html)
    {
        return (string) preg_replace('/<iframe.*?\/iframe>/i', '', $html);
    }

    /**
     * @param $mode
     *
     * @return string
     */
    private function getClassForMode($mode)
    {
        switch ($mode) {
            case self::MODE_TRACKING:
                return 'cookie-settings-tracking';
            case self::MODE_STATS:
                return 'cookie-settings-stats';
            case self::MODE_STATS_AND_TRACKING:
                return 'cookie-settings-stats-and-tracking';
        }

        throw new \InvalidArgumentException(sprintf('Could not find mode %s', $mode));
    }
}
